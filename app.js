let express = require('express');
let app = express();

let vkInstance = undefined;

app.set('views', './tmp');// specify the views directory
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');

app.use(express.static('tmp'));

/** routes ***/
app.get('/', function (req, res) {

    res.render('index')

});

app.listen(8080);