'use strict';
let aMethods = [
    'login',
    'setToken',
    'sendMessage',
    'getMessages',
    'getUpdates'
];

class ABasicClass {
    constructor() {
        if (new.target === ABasicClass) {
            throw new TypeError("Cannot construct Abstract instances directly");
        }
        // aMethods.forEach(function (method) {
        //     if (this[method] === undefined) {
        //         throw new TypeError("All abstract methods should be override");
        //     }
        // });
    }

    login() {
    }

    setToken() {
    }

    sendMessage() {
    }

    getMessages() {
    }

    getUpdates() {
    }
}

module.exports = ABasicClass;