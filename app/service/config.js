'use strict';

module.exports = {
    settings: {
        local_server: 'localhost',
        server_port: '9191'
    },
    for_long_poll: {
        vk: {
            port: '9090',
            service: __dirname + '/../social/vk/service/vkClientService'
        }
    }
};