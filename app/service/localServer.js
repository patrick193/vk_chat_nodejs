'use strict';

const io = require('socket.io');
const config = require('./config');
const route = require('../../router/index');
let socialServers = {};


class localServer {
    constructor() {
        this.host = config.settings.local_server;
        this.port = config.settings.server_port;
    }

    startServers() {
        var s = io.listen(this.port);
        s.sockets.on('connection', function (socket) {
            route.incomeRoutes(socket);
            socket.on('disconnect', function () {
                console.log('disconnect');
                socialServers = {};
            });
        });
        //socials
        var social = config.for_long_poll,
            keys = Object.keys(social);
        keys.forEach(key => {
            let server = io.listen(social[key].port);
            server.on('connection', localSocket => {
                console.log('Connect for local server ' + key);
                localSocket.on('disconnect', () => {
                    console.log('Disconnected form vk');
                });
                let route = require(social[key].service);
                route.events(localSocket);

            });
            socialServers[key] = server;
        });
    }

}

module.exports = localServer;
exports.socialServers = socialServers;
