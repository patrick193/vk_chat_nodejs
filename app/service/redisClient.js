'use strict';
const redis = require('redis');
let instance = null;

class RedisClient {
    constructor() {
        if (!instance) {
            instance = this;
        }
        this.client = undefined;
        this.__client();

        return instance;

    }

    get instance() {
        this.__client();
        return this.client;
    }

    __client() {
        if (instance) {
            this.client = redis.createClient();
        }
    }
}

exports.redisClient = () => {
    return new RedisClient();
};
