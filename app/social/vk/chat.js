'use strict';

const VKApi = require('node-vkapi');
const config = require('../../config');
const aClass = require('../../AClass/ABasicClass');
const vkClient = require('./service/vkClientService');

/**
 * class for chat in vk
 */
class VkChat extends aClass {
    constructor(options = {}) {
        super();
        let params = {
            app: {
                id: config.vk.app_id,
                secret: config.vk.app_secret
            }
        };
        if (options.login && options.pass) {
            params['auth'] = {
                login: options.login,
                pass: options.pass
            };
        }

        this.vk = new VKApi(params);
        this.data = [];
    }

    /**
     * set a token if we already auth
     * @param token
     * @returns {null|*}
     */
    setToken(token) {
        if (this.vk.options.token) {
            return this.vk.options.token;
        }
        this.vk.options.token = token;
        return this.vk.options.token;
    }

    /**
     * login in vk
     * @param callback
     * @param login
     * @param pass
     */
    login(callback, login = '', pass = '') {
        if (!this.vk.options.auth.login || !this.vk.options.auth.pass) {
            this.vk.options.auth.login = login;
            this.vk.options.auth.pass = pass;
        }
        this.vk.auth.user({
            scope: ['audio', 'photos', 'friends', 'wall', 'offline', 'messages']
        }).then(token => {
            this.vk.options.token = token.access_token;
            callback(this.vk.options.token);
        }).catch(err => {
            console.error(err);
        });
    }

    /**
     *
     * @param callback
     * @param token
     * @returns {undefined}
     */
    getMyProfile(callback) {
        if (!this.vk.options.token) {
            return undefined;
        }
        this.vk.call('account.getProfileInfo', {})
            .then((profile) => {
                this.userInfo((data) => {
                    profile.id = data[0].id;
                    profile.photo_50 = data[0].photo_50;
                    callback(profile);
                }, [profile.screen_name, ['photo_50']])
            })
            .catch(err => {
                console.error(err);
            })
    }

    /**
     * get messages from vk
     * @param callback
     * @param count
     */
    getMessages(callback, count = 10) {
        if (!this.vk.options.token) {
            return undefined;
        }
        this.vk.call('messages.get', {
            count: count
        }).then(callback)
            .catch((err) => {
                console.log(err)
            });
    }

    getMessageById(callback, messageId) {
        if (!this.vk.options.token) {
            return undefined;
        }
        this.vk.call('messages.getById', {
            message_ids: messageId
        }).then(callback)
            .catch((err) => {
                console.log(err)
            });
    }

    /**
     *
     * @param callback
     * @param userId
     * @param count
     * @param offset
     * @param startMessageId
     * @param desc
     * @returns {undefined}
     */
    getHistory(callback, userId, count = 10, offset = undefined, startMessageId = undefined, desc = 0) {
        if (!this.vk.options.token) {
            return undefined;
        }
        var params = {
            count: count,
            user_id: userId,
            rev: desc
        };
        startMessageId ? params['start_message_id'] = startMessageId : '';
        offset ? params['offset'] = offset : '';
        this.vk.call('messages.getHistory', params)
            .then(data => {
                var preparedData = {};
                if (startMessageId || offset) {
                    data.paginated = 1;
                }
                preparedData[userId] = data;
                callback(preparedData)
            })
            .catch(err => {
                console.error(err)
            })
    }

    /**
     * send a private messages to user
     * @param callback
     * @param to
     * @param text
     * @returns {undefined}
     */
    sendMessage(callback, options = {}) {
        if (!this.vk.options.token) {
            return undefined;
        }
        //50913749
        this.vk.call('messages.send', {
            user_id: options.to,
            peer_id: options.to,
            message: options.text,
            title: options.title || ''
        }).then(callback)
            .catch(err => {
                console.error(err);
            });
    }

    /**
     * get update from long poll vk server connection
     * here we open a local socket connection for vk server
     * @param socket
     * @param link
     * @param insideSocket
     */
    getUpdates(socket, link = '', insideSocket = undefined) {
        var vkC = new vkClient(insideSocket);
        if (!link) {
            this._getPollServer(vkResponse => {
                if (vkResponse.key) {
                    link = config.vk.poll_server_link;
                    for (var key in vkResponse) {
                        link = link.replace('$' + key, vkResponse[key]);
                    }
                    vkC.updates((localSocket)=> {
                        vkClient.answers(socket, localSocket);
                    }, link, socket);
                }
            });
        } else {
            vkC.updates((localSocket)=> {
                vkClient.answers(socket, localSocket);
            }, link, socket);
        }
    }

    userInfo(callback, userId, fieldsList) {
        if (!this.vk.options.token) {
            return undefined;
        }
        userId = Array.isArray(userId) ? userId.join(',') : userId;
        fieldsList = Array.isArray(fieldsList) ? fieldsList.join(',') : fieldsList;
        this.vk.call('users.get', {
            user_ids: userId,
            fields: fieldsList
        })
            .then(callback)
            .catch(err => {
                console.error(err);
            });
    }

    setAsRead(callback, peerId, messagesId) {
        if (!this.vk.options.token) {
            return undefined;
        }
        var params = {peer_id: peerId};
        messagesId ? params['message_ids'] = messagesId.join(',') : '';
        this.vk.call('messages.markAsRead', params).then(callback)
            .catch(err => {
                console.error(err);
            });
    }

    /**
     * get dialogs
     * @param callback
     * @param count
     * @param unread
     * @param startMessageId
     * @param offset
     * @returns {undefined}
     */
    getDialogs(callback, count = 10, unread = 0, startMessageId = undefined, offset = 0) {
        if (!this.vk.options.token) {
            return undefined;
        }
        var params = {
            count: count,
            unread: unread
        };
        startMessageId ? params['start_message_id'] = startMessageId : '';
        offset ? params['offset'] = offset : '';
        this.vk.call('messages.getDialogs', params)
            .then(callback)
            .catch(err => {
                console.error(err)
            })
    }

    /**
     * getting friends
     * @param callback
     * @param userId
     * @param count
     * @param offset
     * @param fields
     * @returns {undefined}
     */
    getFriends(callback, userId, count = -1, offset, fields) {
        if (!this.vk.options.token) {
            return undefined;
        }

        let params = {
            user_id: userId,
            fields: fields.join(',').toString()
        };

        count > -1 ? params['count'] = count : '';
        offset ? params['offset'] = offset : '';


        this.vk.call('friends.get', params)
            .then(callback)
            .catch(err=> {
                console.error(new Error(err.message));
            })

    }

    /**
     * get poll server
     * @param callback
     * @returns {undefined}
     * @private
     */
    _getPollServer(callback) {
        if (!this.vk.options.token) {
            return undefined;
        }

        this.vk.call('messages.getLongPollServer', {}).then(callback)
            .catch(err => {
                console.error(err);
            });
    }
}
module.exports = VkChat;