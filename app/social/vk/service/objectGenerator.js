'use strict';

let types = {
    1: {
        type_id: 0,
        message_id: 0,
        flags: 0,
        extra_fields: true
    },
    2: {
        type_id: 0,
        message_id: 0,
        mask: 0,
        extra_fields: true
    },
    3: {
        type_id: 0,
        message_id: 0,
        mask: 0,
        extra_fields: true
    },
    4: {
        type_id: 0,
        message_id: 0,
        flags: 0,
        extra_fields: true
    },
    6: {
        type_id: 0,
        peer_id: 0,
        local_id: 0,
        extra_fields: false
    },
    7: {
        type_id: 0,
        peer_id: 0,
        local_id: 0,
        extra_fields: false
    },
    8: {
        type_id: 0,
        user_id: 0,
        extra: 0,
        extra_fields: false
    },
    9: {
        type_id: 0,
        user_id: 0,
        flags: 0,
        extra_fields: false
    },
    10: {
        type_id: 0,
        peer_id: 0,
        mask: 0,
        extra_fields: false
    },
    11: {
        type_id: 0,
        peer_id: 0,
        flags: 0,
        extra_fields: false
    },
    12: {
        type_id: 0,
        peer_id: 0,
        mask: 0,
        extra_fields: false
    },
    51: {
        type_id: 0,
        chat_id: 0,
        self: 0,
        extra_fields: false
    },
    61: {
        type_id: 0,
        user_id: 0,
        flags: 0,
        extra_fields: false
    },
    62: {
        type_id: 0,
        user_id: 0,
        chat_id: 0,
        extra_fields: false
    },
    70: {
        type_id: 0,
        user_id: 0,
        call_id: 0,
        extra_fields: false
    },
    80: {
        type_id: 0,
        count: 0,
        extra_fields: false
    },
    114: {
        type_id: 0,
        peer_id: 0,
        sound: 0,
        disabled_until: false
    }
};

let extra_fields = {
    peer_id: 0,
    timestamp: 0,
    subject: '',
    text: '',
    attachments: [],
    random_id: undefined,
};


class Generator {

    constructor(updates) {
        this.updates = updates;
    }

    get obj() {
        let res = [];
        if (this.updates && Array.isArray(this.updates)) {
            this.updates.forEach(record => {
                if (Array.isArray(record) && types[record[0]]) {
                    var keys = Object.keys(types[record[0]]);
                    let o = {};
                    keys.forEach((d, index) => {
                        if (types[record[0]][d] === true) {
                            let k = Object.keys(extra_fields);
                            o[d] = {};
                            for (var i = 0; i < k.length; i++) {
                                o[d][k[i]] = record[i + index];
                            }
                            return o;
                        } else {
                            o[d] = record[index];
                        }
                    });
                    res.push(o);
                }
            });
        }
        return res;
    }
}

module.exports = Generator;