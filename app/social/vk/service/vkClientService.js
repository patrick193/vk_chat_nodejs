'use strict';

const socialServers = require('../../../service/localServer').socialServers;
const io = require('socket.io-client');
const vkService = require('./vkUpdates').instance();
const redisClient = require('./../../../service/redisClient').redisClient();

class vkClientService {
    constructor() {
        socialServers && socialServers['vk'] ? this.socket = socialServers['vk'] : this.__connect();
    }

    __connect() {
        this.localSocket = io.connect('http://localhost:9090');
        this.localSocket.on('connect', () => {
            if (this.localSocket.connected) {
                return this.localSocket.id
            }
        });
    }

    updates(callback, link, socket = undefined) {
        !this.localSocket ? this.__connect() : '';
        socket.on('disconnect', () => {
            this.localSocket.disconnect();
        });
        this.localSocket.emit('local:vk:updates', {link: link});

        callback(this.localSocket);
    }

    /**
     * events for local server
     * @param localSocket
     */
    static events(localSocket) {
        localSocket.on('local:vk:updates', updates => {
            vkService.check(updates.link, localSocket);
        });
    }

    /**
     * answers from local server to ClientProject
     * @param socket
     * @param localSocket
     */
    static answers(socket, localSocket) {
        localSocket.on('local:fromVK:updates', (data) => {
            socket.emit('vk:updates', {serverId: localSocket.id, info: data})
        });
    }
}

module.exports = vkClientService;
