'use strict';

const request = require('request');
const Generator = require('./objectGenerator');

class vkUpdates {
    check(link, socket) {
        if (socket.connected) {
            request(link, (err, response, body) => {
                body = this.__getObject(body);
                if (body && body.ts) {
                    link = this.__updatePollLongLink(link, body.ts);
                    let gen = new Generator(body.updates);
                    socket.emit('local:fromVK:updates', gen.obj);
                    this.check(link, socket);
                }
            });
        }

    }

    __updatePollLongLink(link, ts) {
        return link.replace(/(ts=([0-9]*))/i, 'ts=' + ts);
    }

    __getObject(data) {
        return JSON.parse(data);
    }
}

// module.exports = vkUpdates;
exports.instance = () => {
    return new vkUpdates();
};