'use strict';
const config = require('./../app/config');
let instance = null;
let Social = null;

class Factory {

    constructor() {
        if (!instance) {
            instance = this;
        }
        this.socket = {};
        this.vk = {
            instance: {},
            login: (callback, user) => {
                this.vk.instance.login(callback, user.login, user.pass);
            },
            send: (callback, data) => {
                this.vk.instance.sendMessage(callback, data);
            },
            updates: (callback) => {
                this.vk.instance.getUpdates(this.socket);
            },
            history: (callback, data)=> {
                this.vk.instance.getHistory(callback, data.userId, data.count, data.offset, data.startMessageId, data.desc);
            },
            users: (callback, data) => {
                this.vk.instance.userInfo(callback, data.userIds, data.fields ? data.fields : '');
            },
            dialogs: (callback, data) => {
                // vk.getDialogs(callback, data.count, data.unread, data.startMessageId, data.offset);
                this.vk.instance.getDialogs(callback, data.count, data.unread, data.startMessageId ? data.startMessageId : undefined, data.offset ? data.offset : undefined);
            },
            message: (callback, data) => {
                this.vk.instance.getMessageById(callback, data.messageId);
            },
            iam: (callback, data) => {
                this.vk.instance.getMyProfile(callback);
            },
            setRead: (callback, data) => {
                this.vk.instance.setAsRead(callback, data.peer_id);
            },
            getUnreadDialogs: (callback, data) => {
                this.vk.instance.getDialogs(callback, data.count, data.unread);
            }
        };

        return instance;
    }

    use(callback, socket, social, event, data) {
        if (this[social]) {
            Social = require(config[social].class);
            this[social].instance = new Social();
            this.socket = socket;
            if (data && data.token) {
                this[social].instance.setToken(data.token);
            }
            return this[social][event](callback, data);
        }
        return callback();

    }
}

exports.f = () => {
    return new Factory();
};