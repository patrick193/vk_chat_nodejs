'use strict';
const factory = require('./factory').f();
const config = require('./../app/config');
let methods = [
    'login',
    'send',
    'updates',
    'history',
    'users',
    'dialogs',
    'message',
    'iam',
    'setRead',
    'getUnreadDialogs',

];

class router {
    static incomeRoutes(socket) {
        var sKeys = Object.keys(config);
        var socketMethods = [];
        sKeys.forEach((sKey) => {
            socketMethods = methods.map(method => {
                return [sKey, method].join(':');
            });
        });
        this.__includeRules(socket, socketMethods)
    }

    static __includeRules(socket, rules) {
        rules.map(rule => {
            return socket.on(rule, (data) => {
                // socket.emit(rule, data)
                this.__factory(socket, rule, data);
            });
        })
    }

    static __factory(socket, socketEvent, data) {
        var promise = new Promise((resolve, reject) => {
            var callback = (data) => {
                resolve(data);
            };
            var arr = socketEvent.toString().split(':'),
                social = arr[0].trim(),
                event = arr[1].trim();
            factory.use(callback, socket, social, event, data);
        });
        promise.then(data => {
            socket.emit(socketEvent, data);
        })

    }
}

module.exports = router;