'use strict';

let vk = require('./app/social/vk/chat');
var fs = require('fs');

let vkInstance = undefined;
let ids = [];
let nodes = [];
let links = [];

!vkInstance ? vkInstance = new vk() : '';
vkInstance.setToken('ad8b1c49ff583348c807e620ba27e4b463ab3c3ccd8b9bf625f38e182db911ff994f8c07d8d8918479eae');
vkInstance.getFriends(friends => {

    // friends.items.forEach(friend => {
    //     ids.push(friend.id);
    // });

    friends.items.forEach((fr, index) => {
        let links1 = (Math.floor(Math.random() * 1000));
        let l = Array.range((Math.floor(Math.random() * 100)), friends.items.length - 1, links1 > friends.items.length - 1 ? friends.items.length - 1 : 13)
        l = shuffle(l);

        var level = Math.floor((Math.random() * 100));
        var score = Math.floor((Math.random() * 100));
        let nodeLevel = level > 4 ? 4 : level;
        let nodeScore = score > 7 ? 7 : score;
        let a = {
            index: index,
            level: nodeLevel,
            score: nodeScore,
            links: l
        };
        fr = Object.assign(fr, a);
        nodes.push(fr);
        links = links.concat(l.map(onelink => {
            return {
                source: index,
                target: onelink,
                weight: Math.random()
            }
        }))
    });

    let a = {nodes: nodes, links: links};
    fs.writeFile('./tmp/data/nodes.json', JSON.stringify(a, null, 4));
    console.log('Finish build');
}, undefined, -1, undefined, [
    'nickname', 'domain', 'sex', 'bdate', 'city', 'country', 'timezone', 'photo_50', 'photo_100', 'photo_200_orig',
    'has_mobile', 'contacts', 'education', 'online', 'relation', 'last_seen', 'status',
    'universities']);
// !vkInstance ? vkInstance = new vk({login: 'patrick1.93@mail.ru', pass: 'AdRVoRM0op'}) : '';
// vkInstance.login(token => {
//     console.log(token);
// });

Array.range = function (a, b, step) {
    var A = [];
    if (typeof a == 'number') {
        A[0] = a;
        step = step || 1;
        while (a + step <= b) {
            A[A.length] = a += step;
        }
    }
    else {
        var s = 'abcdefghijklmnopqrstuvwxyz';
        if (a === a.toUpperCase()) {
            b = b.toUpperCase();
            s = s.toUpperCase();
        }
        s = s.substring(s.indexOf(a), s.indexOf(b) + 1);
        A = s.split('');
    }
    return A;
};

function shuffle(a) {
    for (let i = a.length; i; i--) {
        let j = Math.floor(Math.random() * i);
        [a[i - 1], a[j]] = [a[j], a[i - 1]];
    }

    return a;
}